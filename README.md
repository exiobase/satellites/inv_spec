# Invasive Species Extension

## other sources

https://github.com/NTNU-IndEcol/Invasive_Spec_Footprint

## old

**NOTE TO Stadler, Konstantin :** This is a initial README file with the data provided during the extension project establishment. 
Change and modify the file and the underlying structure as needed. 

Throughout the file several sections are marked with **NOTE:** - these are only for your info and can/should be deleted afterwards. If you need to check the notes at a later stage, check the cookiecutter repository at TODO FILL IN.

**NOTE:** You can change the underlying file structure as it is appropriate for your project. There are only 2 requirements for further processing within the EXIOBASE pipeline:

1. All folders initially established need to stay (you do not need to use all though). Fill in more folders (e.g. 11_raw_auto_aux) as needed in the appropriate places. If you add folders, add them to the structure below and explain their purpose.

2. In the /src directory there needs to be a main.py which contains a function 'run' (it is already in there) - running this function should run the full pipeline to generate a new extension.

**NOTE:** Things to do after establishing a new module project:

1. In the root folder, run 
~~~
git init
git add .
git commit -m "Project init"
~~~

to setup a git repository. The project already contains a .gitignore file with all settings (see folder structure below for more info). 

2. Add a remote repository, preferable at TODO FILL IN

3. Build the conda environment - see [description further down](#Building-the-conda-environment).

4. Start coding


## Extension Description

Invasive species threats due to imports of products, based on characterisation factors based on TODO URL

### Data sources


TODO, not published yet

## Files and Folder Structure

**Note** Below the initial recommendation for setting up the data store. As much as possible follow the structure, but adjust as much as needed. Not all folders need to be filled with data (e.g. perhaps no interpolation is necessary).

**Note** The final data MUST be stored in e3final and e4final to be picked up in the main data pipeline.

```bash
.
+-- raw_data_store          repository for data sent per email or manually downloaded - this is tracked by git
|   |
|   +-- provider_a          downloaded files (e.g. ./raw_data_store/FAO)
|   |   |                   
|   |   +-- desc.md         document manual download date, url, provider (TODO: structured? yaml?)
|   |
|   +-- provider_b          downloaded files (e.g. ./raw_data_store/OECD)
|       |                   
|       +-- desc.md         document manual download date, url, provider (TODO: structured? yaml?)
|
|
+-- data                     all things stored here are not tracked by git - these should all be fully reproducible by the src scripts
|   |
|   +-- auto_down            all automatically downloaded data
|   |   +-- provider_a       (rename to the specific data provider)
|   |   +-- provider_b      
|   |
|   +-- clean_native         cleaning/modifications steps in native classification (can include interpolations/gap filling)
|   |   +-- provider_a      
|   |
|   |
|   +-- e3adj                all data ajustments in exiobase 3 classifiation
|   |   |
|   |   +-- class             original data converted to exiobase3 sector/country classification 
|   |   |
|   |   +-- coeff             exiobase data converted to coefficients (absolute values / monetary output)
|   |   |
|   |   +-- inter             interpolation of missing coefficients over years and regions
|   |   |
|   |   +-- xtra              extrapolations (now-, for- and backcasting)
|   |
|   |
|   +-- e3final               final extension data in exiobase 3 classification
|   |   |
|   |   +-- coeff             coefficient format
|   |   |
|   |   +-- abs               absolute values
|   |
|   +-- e4adj                 all data adjustments in exiobase 4 classifiation
|   |   |
|   |   +-- class             original data converted to exiobase4 sector/country classification 
|   |   |
|   |   +-- coeff             exiobase data converted to coefficients (absolute values / monetary output)
|   |   |
|   |   +-- inter             interpolation of missing coefficients over years and regions
|   |   |
|   |   +-- xtra              extrapolations (now-, for- and backcasting)
|   |
|   |
|   +-- e4final              final extension data in exiobase 4 classification
|       |
|       +-- coeff             coefficient format
|       |
|       +-- abs               absolute values
|
+-- docs                      any extensive documentation to long for the README.md(perfered in markdown)
|
+-- logs                      log files
|
+-- src                       src code modules
|   +-- main.py               main entry point, MUST include a function 'run' 
|   +-- conf.py               configuration and helper functions
|
|
|   Files:
|
+-- environment.yml             all requirments/packages to run - see note below
|
+-- README.md                   this file, should contain the docs (if small) or link to /docs
```

## Building the conda environment

We aim to have a common environment for all extension repositories which is defined in the environment.yml.

To build the environment switch to the inv_spec folder and run

~~~
conda env create -f environment.yml
~~~

and activate with

~~~
conda activate exio_sat_pipeline
~~~

To speed up the process, I recommend to use [mamba](https://github.com/mamba-org/mamba) instead of conda, 
which can be installed in conda with 

~~~
conda install mamba -c conda-forge
~~~

and then the commands is:

~~~
mamba env create -f enviroment.yml
~~~

In any case, the environment is then activated with

~~~
conda activate exio_sat_pipeline
~~~

All scripts/modules should run with this environment.

Once you download a newer version of the environment, you might need to update the environment. 
You can do this with (replace conda with mamba if you use the latter).

~~~
conda env update -f enviroment.yml
~~~

If you need to add packages to the environment, clone the settings repository (TODO URL) and make the changes there. 
Generate a pull request and then inform Konstantin (or any other author of the template repo) to update the repository.
 
## Developers

All persons involved in the development of the extension. 

**NOTE:** Please add as list entries with (only the name is required, but add as many as possible).
Surname, Given Name, Email, Institute GH: github username, GL: gitlab username, orcid: 

- Stadler, Konstantin, konstantin.stadler@ntnu.no, NTNU, GH: konstantinstadler, GL: konstantinstadler

