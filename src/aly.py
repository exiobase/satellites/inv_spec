""" Script for connection exiobase 3 with the final extension

This is just here for the first development
Later, the extension and the analysis should be in different repos

"""

from pathlib import Path
import pymrio
import pandas as pd
import country_converter as coco

exio3 = pymrio.parse_exiobase3("/home/konstans/proj/EXIOBASE/ZENODO_download/382/IOT_2019_pxp.zip")

add_accounts = [
'Value Added', 
'GHG emissions (GWP100) | Problem oriented approach: baseline (CML, 2001) | GWP100 (IPCC, 2007)',
'Land use Crop, Forest, Pasture',
'Water Consumption Blue - Total'
]

ext = Path("/home/konstans/proj/EXIOBASE/exio_satellite/inv_spec/data/e3final/abs/")
inv_spec_ext = pymrio.load(ext)
# reorder columns, that needs to be a function with pymrio later
inv_spec_ext.F = inv_spec_ext.F[exio3.A.columns]



# filter for all rows where the index contains land
F_new = pd.concat([inv_spec_ext.F, exio3.impacts.F.loc[add_accounts,:]])
F_Y_new = pd.concat([inv_spec_ext.F_Y, exio3.impacts.F_Y.loc[add_accounts,:]])
unit_new = pd.concat([inv_spec_ext.unit, exio3.impacts.unit.loc[add_accounts,:]])

# ec.calc_all()

del exio3.satellite
del exio3.impacts

exio3.acc = pymrio.Extension(name='accounts', F=F_new, F_Y=F_Y_new, unit=unit_new)

exio3.calc_all()

mrio_results_folder = Path("/home/konstans/proj/EXIOBASE/exio_satellite/inv_spec/data/e3final/mrio_results/")

exio3.report_accounts(mrio_results_folder / "report_full", format='html')

exio3.acc.save(mrio_results_folder / "accounts_full")


EU_agg = coco.agg_conc(
    original_countries="EXIO3",
    aggregates=["EU27", {"US":"US", "CA":"CA", "RU":"RU", "CN":"CN", "JP":"JP", "IN":"IN", "BR":"BR", "AU":"AU", "ID":"ID"}],
    missing_countries="RoW"
)

exio3_agg = exio3.aggregate(EU_agg)
exio3_agg.calc_all()




exio3_agg.report_accounts(mrio_results_folder / "report_agg", format='html')

exio3_agg.acc.save(mrio_results_folder / "accounts_agg")



