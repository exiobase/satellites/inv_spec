""" Admin and maintenance functions
"""

import re
import sys
import platform
from pathlib import Path

from loguru import logger


class Folders:
    """Getting local folder structure for cross-system access

        This add all first level folders in root as Folders.FOLDERNAME. 'root'.
     Folders can also be nested, e.g. to
    get something like Folders.data.raw. To do so, first run Folders on root, then
    on data and then put them together Folders.data = Data. See conf.py for an
    example on how to do that.

    It also includes an attribute 'folders' which list all paths within root.

        Utility function Folders.mkdir creates a new folders, taking the same
    arguments as Path.mkdir. The folder can either be given absolute (starting with
    the drive letter on windows or / on Linux) or relative. For the latter, the
    folder will be created in the root of Folders. After the creation, the Folders
    instance gets updated with the new folder structure.

    Attributes
    ----------
        root: pathlib.Path
            Root folder of the instance
        folders: list of pathlib.Path
            All first level folders in root
        FOLDERNAME: pathlib.Path
            Each subfolder is also an attribute

    Parameters
    ----------

    root: str or pathlib.Path
        Root folder for the Folder instance.
    """

    def __init__(self, root=Path.cwd()):

        if type(root) is str:
            root = Path(root)
        self.root = root.resolve()

        self.folders = [f for f in list(self.root.glob("*/**")) if f.is_dir()]

        for fol in [f for f in list(self.root.glob("*/")) if f.is_dir()]:
            if fol.name[0] in [".", "_"]:
                continue
            setattr(self, fol.name, fol)

    def __call__(self):
        return self.root

    def __str__(self):
        return f"Folders with root at {self.root}"

    def mkdir(self, path, parents=True, exist_ok=True):
        """Create a new directory and update the instance with the new folder

        Parameters
        ----------
        path: str or pathlib Path
            Directory to create, can be given absolute or relative. Relative ones
            are crated within the instance root.

        parents: boolean, optional
            Default True, create parent directory if they not exist
        exist_ok: boolean, optional
            Default True, do not complain if the folder already exist

        Returns
        -------
        created Path
        """
        if not Path(path).is_absolute():
            path = self.root / Path(path)

        Path(path).mkdir(parents=parents, exist_ok=exist_ok)
        self.__init__(root=self.root)

        return Path(path)


def setup_loggers(log_folder, min_level="DEBUG"):
    """Removes all previous loggers and setup new ones.

    This must be done only once per each run - in main.py at the very beginning.

    It setup 3 loggers:

        1. One to the sytem console with 'min_level' with some basic information
        2. One to a log file (runlog_TIME.log) also at 'min_level' but with more informaton per line
        3. One to a log file (errorlog_TIME.log) with only contains error and exception messages.

    It also puts some first messages logging some system information in the logs and at which files the logs are stored.

    Notes
    -------
    The format and setup of the loggers should not be changed.

    See also
    ---------
    This does not return the loggers, use get_loggers() for that after calling setup_loggers.


    Parameters
    -------------

    log_folder: Pathlib.path
        Where to store the file loggers
    min_level: str, optional
        Minimal level to log into the generic loggers.
        Good choices are "DEBUG" for development and "INFO" for production run.


    """
    logger.remove()

    LOG_FORMAT_CONSOLE = (
        "<green>{time:YYYY-MM-DD HH:mm:ss}</green> | "
        "<level>{level}</level> | "
        "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>"
    )

    LOG_FORMAT_FILE = (
        "{time:YYYY-MM-DD HH:mm:ss} | "
        "{level} | {name}: {module}:{function}:{line} - {message} | "
        "Process: {process.name} - {process.id} | "
        "Thread: {thread.name} - {thread.id}"
    )

    standard_log_file = log_folder / "runlog_{time:YYYYMMDD_HHmmss}.log"
    error_log_file = log_folder / "errorlog_{time:YYYYMMDD_HHmmss}.log"

    logger.add(
        sys.stdout, format=LOG_FORMAT_CONSOLE, level=min_level, enqueue=True, catch=True
    )

    logger.add(
        sink=standard_log_file,
        format=LOG_FORMAT_FILE,
        level=min_level,
        serialize=True,
        enqueue=True,
        catch=True,
    )
    logger.add(
        sink=error_log_file,
        format=LOG_FORMAT_FILE,
        level="ERROR",
        backtrace=True,
        diagnose=True,
        enqueue=True,
        catch=True,
    )

    logger.info("Log files are stored at {}".format(log_folder))
    logger.info("Main node running on {}".format(platform.uname()))
    logger.info("Running on Python version {}".format(platform.python_version()))


def get_loggers():
    """Get exisiting logger objects.

    This can be used in each submodules to get the module/project level loggers.
    """
    return logger
