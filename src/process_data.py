"""
Process the data for the Invasive Species Extension.


"""

import pandas as pd
import numpy as np
import country_converter as coco
import pymrio

import conf

logger = conf.logger

logger.info("Loading invasive raw data.")
raw = pd.read_csv(
    conf.data.auto_down / "BACI_HS92_Y2019_V202102_impacts.csv", sep=";", decimal=",", 
    usecols=["t", "v", "commodity", "importer", "exporter", "PDFyrs", "PDFyrs_marginal"],
    dtype={"t": int, "v": float, "commodity": str, "importer": str, "exporter": str, "PDFyrs": float, "PDFyrs_marginal": float})


invasives  = raw.copy()
invasives.v = invasives.v * 1000
invasives.rename(columns={"v": "USD"}, inplace=True)
# invasives = invasives.sample(n=1000, random_state=42)



logger.info("Loading exiobase 3 pxp 2019.")
exio3 = pymrio.parse_exiobase3("/home/konstans/proj/EXIOBASE/ZENODO_download/382/IOT_2019_pxp.zip")

logger.info("Loading concordance.")
# TODO: review concordance - hs needs to be updates, newer hs version and exio version, fix nan
hs_exio_conc = (
    pd.read_excel(
        conf.proj.raw_data_store / "HS_EXIO.xlsx",
        sheet_name="hs1996_exiobase_bridge",
        dtype=str,
    )
    .loc[:, ["hs1996", "Exiobase_30"]]
    .dropna(axis=0)
)

logger.info("Converting invasive species data to EXIOBASE codes")

# replace commodity with exiobase code
invasives.reset_index(inplace=True)

invasives["commodity"] = invasives["commodity"].replace(
    hs_exio_conc.set_index("hs1996").Exiobase_30
)

not_fd_conc = invasives[invasives.commodity.str[0] != "p"]

if not_fd_conc.size > 0:
    logger.warning(f"Found {len(not_fd_conc.commodity.unique())} hs codes not in concordance")
    logger.warning(f"Missing entries: {not_fd_conc.commodity.size}")
    logger.warning(f"Missing trade: {not_fd_conc.USD.sum()/1E6:.2f} Mill USD")
    logger.warning(f"Missing PDFyrs: {not_fd_conc.PDFyrs.sum()}")
    invasives = invasives[invasives.commodity.str[0] == "p"]
else:
    logger.info("All hs codes in concordance found")


invasives = invasives.loc[:, ["commodity", "importer", "exporter", "PDFyrs", "PDFyrs_marginal"]]

logger.info("Converting to EXIOBASE country codes")

cc = coco.CountryConverter()
invasives["importer"] = cc.pandas_convert(invasives["importer"], src='iso3', to='EXIO3', not_found=None)
invasives["exporter"] = cc.pandas_convert(invasives["exporter"], src='iso3', to='EXIO3', not_found=None)
inv_exio3 = invasives[(invasives.importer.isin(cc.EXIO3.EXIO3) & (invasives.exporter.isin(cc.EXIO3.EXIO3)))]

# get all countries that are not in exiobase 3 country list
if len(inv_exio3) != len(invasives):
    logger.warning(f"Countries not found in the country converter correspondance") 
    logger.warning(f"Missing entries: {len(invasives) - len(inv_exio3)}")
    logger.warning(f"Missing PDFyrs: {invasives[~invasives.index.isin(inv_exio3.index)].PDFyrs.sum()}")
else:
    logger.info("All countries in concordance found")

# check for nan in PDFyrs and PDFyrs_marginal

if inv_exio3.PDFyrs.isna().any():
    logger.warning(f"Found {inv_exio3.PDFyrs.isna().sum()} nan in PDFyrs")

if inv_exio3.PDFyrs_marginal.isna().any():
    logger.warning(f"Found {inv_exio3.PDFyrs_marginal.isna().sum()} nan in PDFyrs_marginal")

inv_exio3 = inv_exio3.dropna(axis=0)

logger.info("Rename to exiobase 3 string names")

exio_code_conv = pd.read_csv(conf.exio3_sectors_file, sep='\t', index_col=0)[['ExioName', 'ExioCode']].set_index('ExioCode')
inv_exio3.commodity = inv_exio3.commodity.replace(exio_code_conv.ExioName)
# check if all commodities where replaced
assert inv_exio3.commodity.isin(exio_code_conv.ExioName).all(), "Not all commodity names where replaced"




# prepare extension template
F = pd.DataFrame(index=['PDFyrs', 'PDFyrs_marginal'], columns=exio3.satellite.F.columns, data=0)
F.index.name = 'stressor'
F_Y = pd.DataFrame(index=['PDFyrs', 'PDFyrs_marginal'], columns=exio3.satellite.F_Y.columns, data=0)
F_Y.index.name = 'stressor'

for co in exio3.get_regions():

    logger.info(f"Processing {co}")

    prod_recipe_abs = exio3.Z.loc[:, co].merge(exio3.Y.loc[:, co], left_index=True, right_index=True)
    # This includes the domestic part as we need that for the RoW regions which imports/exports within region.
    # For the other countries this should not matter as it is 0 anyway.
    prod_recipe_shares = prod_recipe_abs.div(prod_recipe_abs.sum(axis=1), axis=0)
    prod_recipe_shares.dropna(axis=0, inplace=True)

    for account in F.index:

        inv_co = inv_exio3[inv_exio3.importer == co].set_index(['exporter', 'commodity']).loc[:, account]
        inv_co.index.names = prod_recipe_shares.index.names

        impact = prod_recipe_shares.mul(inv_co, axis=0).fillna(0).sum(axis=0)

        impact_F = impact[impact.index.isin(F.columns.get_level_values('sector'))]

        impact_F_Y = impact[impact.index.isin(F_Y.columns.get_level_values('category'))]

        F.loc[account, (co, impact_F.index)] = impact_F.values
        F_Y.loc[account, (co, impact_F_Y.index)] = impact_F_Y.values


        # check if all values where replaced
        diff_distributed = inv_co.sum() - (F.loc[account, co].sum() + F_Y.loc[account, co].sum())


        if not np.isclose(diff_distributed, 0, atol=np.spacing(inv_co.sum()), rtol=0):
            logger.warning(f"Difference in distributed values: {diff_distributed}")
            logger.warning(f"Might be due to import shares which don’t receive impacts")


unit = pd.DataFrame(index=F.index, columns=['unit'], data=['PDF', 'PDF'])


inv_extension = pymrio.Extension(name='invasive_species', F=F, F_Y=F_Y, unit=unit)

# TODO - abs should be attribute - or leave that out of the template
result_folder = conf.data.e3final / "abs"
logger.info(f"Writing results to to {result_folder}")
inv_extension.save(conf.data.e3final / "abs")

# TODO: move conversion functino to ex2mag
# ex2mag will have a function, class_converter and country_convert which takes the data and converts it to EXIOBASE3 format, and aggregates



# TODO: 88 duplicates in hs codes - would need to be split up for the concordance to work
# hs_exio_conc[hs_exio_conc.hs1996.duplicated()].size

# check if all commodity in r are in hs_exio_conc
# imp_impacts[~imp_impacts.index.get_level_values("commodity").isin(hs_exio_conc.hs1996)]
# TODO: there are many (around 3 %) which are not in the concordance, probably due to hs1996


# TODO: Taiwan is missing in the data, doublecheck - could be split from China





