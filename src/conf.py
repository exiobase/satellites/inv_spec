""" Configuration settings and utility functions for Invasive Species Extension.

The file in split into sections, look for ## SECTION  to jump to them.

"""


from pathlib import Path
import util.admin


## SECTION FOLDERS

# Root folder of the extension project
proj = util.admin.Folders(root=Path(__file__).parent / "..")

# Data folders, usually within root but could also be at a remote data lake or similar
data = util.admin.Folders(proj.root / "data")


##  SECTION RAW DATA

raw_data_ntnu = Path('/media/ntnu/Xdrive/indecol/USERS/JanB/Invasive_Spec_Footprint/BACI_HS92_Y2019_V202102_impacts.csv')
exio3_sectors_file = proj.raw_data_store / "exio3_pxp_sectors.tsv"



## SECTION LOGGING
util.admin.setup_loggers(log_folder=proj.logs, min_level="DEBUG")
# util.admin.setup_loggers(log_folder=Path("../logs"), min_level="DEBUG")

logger = util.admin.get_loggers()
