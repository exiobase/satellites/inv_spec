"""
Main entry point for compiling the Invasive Species Extension.

"""

import shutil

import conf

logger = conf.logger


def run():
    """Main function to run to compile the complete extension."""
    logger.info("Start building the Invasive Species Extension data")

    raw_data = get_data_ntnu(conf.raw_data_ntnu, conf.data.auto_down)
    return locals()


def get_data_ntnu(src_file, dst_folder):
    """ Get data from NTNU
    """
    logger.info("Getting data from NTNU")
    result_file = dst_folder / src_file.name
    if not result_file.exists():
        logger.info(f"Copying {src_file} to {result_file}")
        shutil.copy(src_file, result_file)
    else:
        logger.info(f"File {result_file} already exists, skipping copy")
    logger.info("Done getting data from NTNU network drive")
    return dst_folder / src_file.name

if __name__ == "__main__":
    try:
        locals().update(run())
    except Exception as e:
        raise
